<?php

namespace App\Request;

interface RequestInterface
{
    public function getMethod(): string;

    public function getEndpoint(): string;

    public function getRequestParam(): ?string;

    public function getQueryParams(): array;

    public function getBodyParams(): array;
}