<?php

namespace App\Request;

class BaseRequest implements RequestInterface
{
    /**
     * @var string
     */
    protected $method = 'GET';

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var string
     */
    protected $requestParam = '';

    /**
     * @var string|null
     */
    protected $token = null;

    /**
     * @var array
     */
    protected $queryParams = [];

    /**
     * @var array
     */
    protected $bodyParams = [];

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    public function getRequestParam(): ?string
    {
        return $this->requestParam;
    }

    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    public function getBodyParams(): array
    {
        return $this->bodyParams;
    }

    public function hasQueryParams(): bool
    {
        return !empty($this->queryParams);
    }

    public function hasBodyParams(): bool
    {
        return !empty($this->bodyParams);
    }
}