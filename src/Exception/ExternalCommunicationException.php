<?php

namespace App\Exception;

class ExternalCommunicationException extends \Exception
{
    protected $message = 'Error on external communication with 3rd party API';

    // protected $code = ResponseService::HTTP_REMOTE_COMMUNICATION_ERROR;

    public function __construct($message = null, $code = null, \Throwable $previous = null)
    {
        $intendedMessage = $this->message;

        if (isset($message)) {
            $intendedMessage .= '; external message: ' . $message;
        }

        if (!isset($code)) {
            $code = $this->code;
        }

        parent::__construct($intendedMessage, $code, $previous);
    }
}