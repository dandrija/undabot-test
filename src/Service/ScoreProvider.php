<?php

namespace App\Service;

use App\Handler\RequestHandlerInterface;
use App\Request\RequestInterface;

abstract class ScoreProvider implements ScoreProviderInterface
{
    /**
     * @var RequestHandlerInterface
     */
    private $requestHandler;

    /**
     * ScoreProvider constructor.
     * @param RequestHandlerInterface $requestHandler
     */
    public function __construct(RequestHandlerInterface $requestHandler)
    {
        $this->requestHandler = $requestHandler;
    }

    protected function fetchResultsFromApi(RequestInterface $request)
    {
        $response = $this->requestHandler->handle($request);

        return $response->toArray();
    }

    /**
     * @param int $positiveResults
     * @param int $negativeResults
     * @return float
     */
    protected function calculateScore(int $positiveResults, int $negativeResults) : float
    {
        $totalResults = $positiveResults + $negativeResults;

        return $positiveResults / $totalResults * 10;
    }
}