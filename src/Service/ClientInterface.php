<?php


namespace App\Service;


use App\Request\RequestInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

interface ClientInterface
{
    public function request(RequestInterface $request): ResponseInterface;
}