<?php

namespace App\Service;

use App\Exception\ExternalCommunicationException;
use App\Request\RequestInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class HttpApiClient implements ClientInterface
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * HttpApiClient constructor.
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws ExternalCommunicationException
     */
    public function request(RequestInterface $request): ResponseInterface
    {
        try {
            $response = $this->client->request(
                $request->getMethod(),
                $this->routeConstructor(
                    $request->getUrl(),
                    $request->getEndpoint(),
                    $request->getRequestParam()
                ),
                $this->addParams($request)
            );
        } catch (TransportExceptionInterface $exception) {
            throw new ExternalCommunicationException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getPrevious()
            );
        }

        return $response;
    }

    /**
     * @param RequestInterface $request
     *
     * @return array
     */
    private function addParams(RequestInterface $request): array
    {
        $query = [];

        if ($request->hasQueryParams()) {
            foreach ($request->getQueryParams() as $key => $queryParam) {
                $query['query'][$key] = $queryParam;
            }
        }

        if ($request->hasBodyParams()) {
            foreach ($request->getBodyParams() as $key => $bodyParam) {
                $query['json'][$key] = $bodyParam;
            }
        }

        return $query;
    }

    /**
     * @param string $url
     * @param string $routePath
     * @param string $getParam
     *
     * @return string
     */
    private function routeConstructor(string $url, string $routePath, string $getParam): string
    {
        return $url . $routePath . $getParam;
    }
}