<?php

namespace App\Service;

interface ScoreProviderInterface
{
    public function fetchScore(string $term): float;
}