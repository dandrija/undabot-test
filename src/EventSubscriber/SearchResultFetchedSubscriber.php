<?php

namespace App\EventSubscriber;

use App\Event\SearchResultFetchedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SearchResultFetchedSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SearchResultFetchedSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|string[]
     */
    public static function getSubscribedEvents()
    {
        return [
            SearchResultFetchedEvent::class => 'onSearchResultFetched',
        ];
    }

    /**
     * @param SearchResultFetchedEvent $event
     */
    public function onSearchResultFetched(SearchResultFetchedEvent $event)
    {
        $this->entityManager->persist($event->getSearchResult());
        $this->entityManager->flush();
    }
}