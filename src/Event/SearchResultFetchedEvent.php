<?php

namespace App\Event;

use App\Entity\SearchResult;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Contracts\HttpClient\ResponseInterface;

class SearchResultFetchedEvent extends Event
{
    /**
     * @var ResponseInterface
     */
    private $result;

    /**
     * SearchResultFetchedEvent constructor.
     * @param SearchResult $result
     */
    public function __construct(SearchResult $result)
    {
        $this->result = $result;
    }

    public function getSearchResult(): SearchResult
    {
        return $this->result;
    }
}