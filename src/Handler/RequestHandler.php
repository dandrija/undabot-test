<?php

namespace App\Handler;

use App\Exception\ExternalCommunicationException;
use App\Request\RequestInterface;
use App\Service\HttpApiClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RequestHandler implements RequestHandlerInterface
{
    /**
     * @var HttpApiClient
     */
    private $client;

    /**
     * RequestHandler constructor.
     *
     * @param HttpApiClient $client
     */
    public function __construct(HttpApiClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     * @throws ExternalCommunicationException
     */
    public function handle(RequestInterface $request): ResponseInterface
    {
        try {
            $response = $this->client->request($request);
        } catch (TransportExceptionInterface $exception) {
            throw new ExternalCommunicationException(
                'External communication error',
                $exception->getCode(),
                $exception
            );
        }

        return $response;
    }
}