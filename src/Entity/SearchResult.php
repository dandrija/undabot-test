<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Action\NotFoundAction;
use App\Repository\SearchResultRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get" = {
 *              "method"="GET",
 *              "controller"=NotFoundAction::class,
 *              "read"=false,
 *              "output"=false,
 *          }
 *     },
 *     collectionOperations={
 *         "api_score"={
 *              "method"="GET",
 *              "route_name"="api_score"
 *         }
 *     },
 *     attributes={"pagination_enabled"=false}
 *     )
 * @ApiFilter(SearchFilter::class, properties={"term"="exact"})
 * @ORM\Entity(repositoryClass=SearchResultRepository::class)
 */
class SearchResult
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @Assert\NotBlank
     *
     * @ApiProperty(identifier=true)
     */
    private $term;

    /**
     * @ORM\Column(type="float")
     *
     * @Assert\Range(
     *      min = 0.0,
     *      max = 10.0,
     *      notInRangeMessage = "Score must be between 0 and 10",
     * )
     */
    private $score;

    /**
     * SearchResult constructor.
     * @param $term
     * @param $score
     */
    public function __construct($term, $score)
    {
        $this->setTerm($term);
        $this->setScore($score);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTerm(): ?string
    {
        return $this->term;
    }

    public function setTerm(string $term): self
    {
        $this->term = $term;

        return $this;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore(float $score): self
    {
        $this->score = $score;

        return $this;
    }
}
