<?php

namespace App\Tests\Unit\Entity;

use App\Entity\SearchResult;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SearchResultTest extends TestCase
{
    /**
     * @var RecursiveValidator|ValidatorInterface
     */
    private $validator;

    public function setUp() : void
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();
    }

    public function testCreatingSearchResult()
    {
        $searchResult = new SearchResult('php', 5);

        $this->assertSame(5.0, $searchResult->getScore());

        $errors = $this->validator->validate($searchResult);

        $this->assertSame(0, count($errors));
    }

    public function testScoreCanNotBeGreaterThanTen()
    {
        $searchResult = new SearchResult('php', 11);

        $errors = $this->validator->validate($searchResult);

        $this->assertSame(1, count($errors));
    }

    public function testScoreCanNotBeLowerThanZero()
    {
        $searchResult = new SearchResult('php', -1);

        $errors = $this->validator->validate($searchResult);

        $this->assertSame(1, count($errors));
    }
}
